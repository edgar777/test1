require('dotenv').config();
const express=require('express');
const rout=express.Router();
const jwt=require('jsonwebtoken');


const SQL=require('../../config.js').pool;
const {authentication}=require('../../authentication.js');

rout.get('/', authentication, (req,resp)=>{
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRT, async(err)=>{
        if(err)return resp.status(401).json({success: false, error: 'invalid token'});
        try{
            const get_task=await SQL.query('SELECT * FROM task');
            const list=get_task[0];
            resp.json({success:true,list});
        }
        catch(err){
            resp.status(403).json({ err: 'Something Was Wrong, Try Again'});
        }
    })
})

rout.post('/create', authentication, (req,resp)=>{
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRT, async(err)=>{
        if(err)return resp.status(401).json({success: false, error: 'invalid token'});
        try{
            const {task}=req.body;
            await SQL.query('INSERT INTO task (title,date) VALUES (?,?)',[task.title,task.date]);
            resp.json({success:true});
        }
        catch(err){
            resp.status(403).json({ err: 'Something Was Wrong, Try Again'});
        }
    })
})

rout.delete('/delete/', authentication, (req,resp)=>{
    jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRT, async(err)=>{
        if(err)return resp.status(401).json({success: false, error: 'you dont have access'});
        try{
            const {taskID}=req.query;
            await SQL.query('DELETE FROM task WHERE id=?',[taskID]);
            resp.json({success:true});
        }
        catch(err){
            resp.status(403).json({ err: 'Something Was Wrong, Try Again'});
        }
    })
})




module.exports=rout;