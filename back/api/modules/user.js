require('dotenv').config();
const express=require('express');
const rout=express.Router();
const jwt=require('jsonwebtoken');

const {authorization}=require('../../authorization.js');
const USER=require('../foo/user.js');

rout.post('/register', authorization, async(req,resp)=>{
    try{
        const {email,pass}=req.body;
        const check_email=await USER.checkEmail(email);
        if(check_email)return resp.json(USER.ERR_EXIST_MAIL);
        if(check_email.err)throw 'err';
        const insert_user=await USER.insert(email,pass);
        if(!insert_user.err)return resp.json(USER.SUCCESS);
        else throw 'err';
    }   
    catch(err){
        resp.status(403).json({ err: 'Something Was Wrong, Try Again'});
    }
})


rout.post('/login', authorization, async(req,resp)=>{
    try{
        const {email,pass}=req.body;
        const check_valid=await USER.checkValid(email,pass);
        if(!check_valid)return resp.json(USER.WRONG);
        const user=check_valid;
        delete user.pass;
        let accessToken=jwt.sign({email,pass}, process.env.ACCESS_TOKEN_SECRT, {expiresIn:'720h'});
        resp.json({user,accessToken});
    }   
    catch(err){
        resp.status(403).json({ err: 'Something Was Wrong, Try Again'});
    }
})

module.exports=rout;