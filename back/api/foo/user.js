const SQL=require('../../config.js').pool;
const crypto=require('crypto');
const sha256=x=>crypto.createHash('sha256').update(x).digest('hex');


const ERR_EXIST_MAIL={success: false, error: "email is already taken"};
const SUCCESS={success: true};
const WRONG={success: false, error: "Wrong Username Or Password"};

const checkEmail=async(email)=>{
    try{
        const check_email=await SQL.query('SELECT id FROM user WHERE email=?',[email]);
        if(check_email[0][0]!==undefined)return true;
        else return false;
    }
    catch(err){
        return {err:err.message};
    }
}

const insert=async(email,pass)=>{
    try{
        await SQL.query(`INSERT INTO user (email,pass) VALUES (?,?)`, [email,sha256(pass)]);
        return true;
    }
    catch(err){
        return {err:err.message};
    }
}


const checkValid=async(email,pass)=>{
    try{
        const get_user=await SQL.query('SELECT * FROM user WHERE email=? and pass=?',[email,sha256(pass)]);
        if(get_user[0].length===0)return false;
        else return get_user[0][0];
    }
    catch(err){
        return {err:err.message};
    }
}





module.exports={checkEmail,insert,checkValid,ERR_EXIST_MAIL,SUCCESS,WRONG}