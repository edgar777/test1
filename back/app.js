require('dotenv').config();
const PORT=process.env.PORT;
const express=require('express');
const app=express();
const path=require('path');

// Api modules
const api_user=require('./api/modules/user.js');
const api_task=require('./api/modules/task.js');

// Usage
app.use('/public', express.static(path.join(__dirname, 'api')));
app.use(express.json({limit: '50MB', extended: true}));
app.use('/api',(req, res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Authorization, Content-Type, Accept");
    next();
});

//Routing Api Usage
app.use('/api/user', api_user);
app.use('/api/task', api_task);


app.listen(PORT);