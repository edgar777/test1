require('dotenv').config();
const mysql=require('mysql2');
const mysql2=require('mysql2/promise');
const DB_Connect=mysql.createConnection({
        host:process.env.config_host,
        user:process.env.config_user,
        password:process.env.config_pass,
        database:process.env.config_db,
    });

const pool = mysql2.createPool({
    host:process.env.config_host,
    user:process.env.config_user,
    password:process.env.config_pass,
    database:process.env.config_db,
});   


module.exports={DB_Connect,pool};
